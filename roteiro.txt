git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init: inicializa um novo repositório de código-fonte no diretório atual.

git status: mostra o estado atual do repositório.

git add arquivo/diretório : adiciona um arquivo específico ao índice.
git add --all = git add . : adiciona arquivos ao índice, preparando-os para serem incluídos em um commit futuro.

git commit -m “Primeiro commit” : adiciona uma nova confirmação (commit) ao histórico do repositório, rastreando as alterações realizadas desde o último commit.

-------------------------------------------------------

git log : exibe o histórico de commits do repositório.
git log arquivo : mostra um commit específico
git reflog : exibe o histórico de referências movidas ou apagadas. Ele mostra todos os movimentos de referência, incluindo branches e tags, e quando eles foram movidos ou apagados.

-------------------------------------------------------

git show : exibe informações sobre o último commit.
git show <commit> :  exibe informações sobre um commit específico.

-------------------------------------------------------

git diff : mostra as diferenças entre as alterações não rastreadas e o último commit.
git diff <commit1> <commit2> : exibe as diferenças entre as versões atuais dos arquivos no diretório de trabalho e as versões registradas no índice ou no último commit. 

-------------------------------------------------------

git branch : Lista todos os branches existentes no repositório, incluindo o branch atualmente selecionado
git branch -r : Lista somente os branches remotos.
git branch -a : Lista todos os branches existentes, incluindo os branches remotos.
git branch -d <branch_name> :  Exclui um branch existente. Ele só pode ser excluido se já tenha sido mesclado com outro branch.
git branch -D <branch_name> : Força a exclusão de um branch, mesmo que ele não tenha sido mesclado.
git branch -m <nome_novo> : renomeia o branch atual para o nome especificado.
git branch -m <nome_antigo> <nome_novo> : Renomeia um branch existente.

-------------------------------------------------------

git checkout <branch_name> : Muda para o branch especificado. Ele irá mudar o conteúdo dos arquivos no diretório de trabalho para a versão do branch selecionado.
git checkout -b <branch_name> : Cria um novo branch com o nome especificado e muda para ele.

-------------------------------------------------------

git merge <branch_name> : Funde as alterações do branch especificado com o branch atualmente selecionado.

-------------------------------------------------------

git clone : cria uma cópia de um repositório existente em outra localização. Ele baixa todo o histórico de commits, branches e arquivos do repositório original e cria um novo repositório local com esses dados.
git pull : combina automaticamente as alterações de um repositório remoto com o repositório local.
git push : envia as alterações de um repositório local para um repositório remoto. Ele envia as confirmações (commits) do branch atualmente selecionado no repositório local para o repositório remoto, atualizando a versão remota do branch com as alterações mais recentes.

-------------------------------------------------------

git remote : Lista todos os repositórios remotos configurados no repositório local.
git remote -v : Lista todos os repositórios remotos configurados no repositório local junto com as URLs deles.
git remote add origin <url> : adiciona um novo repositório remoto com o nome "origin" e a url especificada.
git remote rename <old-name> <new-name> : Renomeia um repositório remoto existente.

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLxjCnO-f9JPT6Kuww_d2TJQK31Mz-cwR4